import React from 'react'
import { Input, Checkbox, Dropdown } from 'semantic-ui-react'
import styled from 'styled-components'
import axios from 'axios'

export const Page = styled.div`
  margin: 0 auto;
  width: 50%;
  padding: 40px;
`

export const Header = styled.div`
display: flex;
`

export const Content = styled.div``

export const Container = styled.div`
margin: 20px 0;
`

export const VideoWrapper = styled.div`
  display: flex;
`

export const Thumbnail = styled.img`
height: 300px;
`

export const TextWrapper = styled.div``

export const Link = styled.a``

export const Text = styled.div``

export const SearchContainer = styled.div`
  margin-right: 20px;
`
export const CheckboxContainer = styled.div`
display: flex;
align-items: center;
`

const CHANNELS = [
  {
    key: 'cnn',
    text: 'CNN',
    value: 'cnn'
  },
  {
    key: 'fox_news',
    text: 'Fox news',
    value: 'fox_news'
  },
  {
    key: 'hoh',
    text: 'hoh',
    value: 'hoh'
  },
  {
    key: 'espn',
    text: 'espn',
    value: 'espn'
  },
  {
    key: 'cnbc',
    text: 'cnbc',
    value: 'cnbc'
  }
]
const YEARS = new Array(12).fill(2010).map((val, index) => {
  return {
    key: val + index,
    text: val + index,
    value: val + index
  }
})
const Video = ({link, pub_date, thumbnail, title}) => {
  return (
    <VideoWrapper>
      <Link href={link} target="_blank"><Thumbnail src={thumbnail}></Thumbnail></Link>
      <Link href={link} target="_blank"><Text>{title}</Text></Link>
      <TextWrapper>{pub_date}</TextWrapper>
    </VideoWrapper>
  )
}
const mock = [{link: "https://www.youtube.com/watch?v=Efv0lgcSDgY",
        pub_date: "2020-03-06T07:00:05Z",
        thumbnail: "https://i.ytimg.com/vi/Efv0lgcSDgY/hqdefault.jpg",
        title: "Steph Curry returns with 23 points, 7 rebounds and 7 assists | 2019-20 NBA Highlights"}]
class VideoSearch extends React.Component {

  state = {
    startYear: 2020,
    endYear: 2021,
    searchText: '',
    channel: 'espn',
    getAll: false,
    data: []
  }

  componentDidMount() {
    this.getVideos()
  }

  getVideos = () => {
    const {startYear,
      endYear,
      searchText,
      channel,
      getAll } = this.state
    
    const getAllParam = getAll ? 1 : 0
    axios.get(`https://rewindnews.ml/getVid?start=${startYear}&end=${endYear}&q=${searchText}&channel=${channel}&getall=${getAllParam}`)
      .then( (response) => {
        // handle success
        console.log(response);
        
        this.setState({
          data: response.data.videos
        })
      })
      .catch( (error) => {
        // handle error
        console.log(error);
        this.setState({
          data: mock
        })
      })

  }

  handleSearch = (e) => {
    const value = e.currentTarget.value

    this.setState({
      searchText: value
    })
  }

  handleClick = () => {
    this.getVideos()
  }

  renderSearch = () => {
    return (
      <SearchContainer>
        <Input action={{ icon: 'search', onClick: () => this.handleClick()}} placeholder='Search...' onChange={this.handleSearch}/>
      </SearchContainer>
    )
  }

  renderDateRange = () => {
    const { startYear, endYear } = this.state
    return (
      <>
        <Dropdown
          placeholder='Select start year'
          button
          className='icon'
          floating
          labeled
          icon='world'
          onChange={this.handleDropdown('startYear')}
          value={startYear}
          options={YEARS}
      />
        <Dropdown
        placeholder='Select end year'
        button
        className='icon'
        floating
        labeled
        value={endYear}
        icon='world'
        onChange={this.handleDropdown('endYear')}
        options={YEARS}
      />
    </>
    )
  }

  handleDropdown = (name) => (e, data) => {
    const value = data.value

   this.setState({
     [name]: value
   }, this.getVideos)
  }

  renderChannels = () => {
    const { channel } = this.state
    return (
      <Dropdown
        placeholder='Select channel'
        button
        className='icon'
        floating
        labeled
        value={channel}
        onChange={this.handleDropdown('channel')}
        icon='world'
        options={CHANNELS}
      />
    )
  }

  handleCheck = () => {
    this.setState({
      getAll: this.state.getAll ? false : true
    }, this.getVideos)
  }

  renderGetAll = () => {
    const { getAll } = this.state
    return (
      <CheckboxContainer>
        <Checkbox label='Get all' checked={getAll} onClick={this.handleCheck}/>
      </CheckboxContainer>
    )
  }

  renderVideos = () => {
    const { data } = this.state
    return (
      <Container>
        {data.length > 0 && data.map(d => {
          const { link, pub_date, thumbnail, title} = d
          return (
            <Video link={link} pub_date={pub_date} thumbnail={thumbnail} title={title} />
          )
        })}

      </Container>
    )
  }

  render() {
    return(
      <Page>
        <Header>
          {this.renderSearch()}
          {this.renderChannels()}
          {this.renderDateRange()}
          {this.renderGetAll()}
          </Header>
        <Content>{this.renderVideos()}</Content>
      </Page>
    )
  }
}

export default VideoSearch