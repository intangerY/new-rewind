import 'semantic-ui-css/semantic.min.css'
import VideoSearch from './VideoSearch'

export default function Home() {
  return (
    <VideoSearch />
  )
}
